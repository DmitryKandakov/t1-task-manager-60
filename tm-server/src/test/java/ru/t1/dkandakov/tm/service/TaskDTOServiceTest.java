package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.marker.DBCategory;

@Category(DBCategory.class)
public class TaskDTOServiceTest extends AbstractDTOServiceTest {

    @NotNull
    private final String userId = "userId-1";

    @NotNull
    private final String userId2 = "userId-2";

    @Test
    @Category(DBCategory.class)
    public void testChangeTaskStatusById() {
        taskService.removeAll();
        taskService.create(userId, "Task name", "Task description");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals(Status.NOT_STARTED, taskService.findOneById(userId, id).getStatus());
        Assert.assertNotNull(taskService.changeTaskStatusById(userId, id, Status.IN_PROGRESS));
        Assert.assertEquals(Status.IN_PROGRESS, taskService.findOneById(userId, id).getStatus());
    }

    @Test
    @Category(DBCategory.class)
    public void testClear() {
        taskService.removeAll();
        @NotNull final TaskDTO task = new TaskDTO();
        taskService.create(userId, "Task name 1", "Task description 1");
        taskService.create(userId, "Task name 2", "Task description 2");
        taskService.create(userId2, "Task name 3", "Task description 3");
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        taskService.removeAll(userId);
        Assert.assertEquals(0, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
    }

    @Test
    @Category(DBCategory.class)
    public void testCreate() {
        taskService.removeAll();
        taskService.create(userId, "Task name 1", "Task description 1");
        Assert.assertFalse(taskService.findAll().isEmpty());
        Assert.assertEquals("Task name 1", taskService.findAll(userId).get(0).getName());
        Assert.assertEquals("Task description 1", taskService.findAll(userId).get(0).getDescription());
        Assert.assertEquals(userId, taskService.findAll().get(0).getUserId());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindAll() {
        taskService.removeAll();
        taskService.create(userId, "Task name 1", "Task description 1");
        taskService.create(userId, "Task name 2", "Task description 2");
        taskService.create(userId2, "Task name 3", "Task description 3");
        Assert.assertEquals(2, taskService.findAll(userId).size());
        Assert.assertEquals(1, taskService.findAll(userId2).size());
        Assert.assertEquals(3, taskService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void testFindOneById() {
        taskService.removeAll();
        @NotNull TaskDTO task1 = taskService.create(userId, "Task name 1", "Task description 1");
        @NotNull TaskDTO task2 = taskService.create(userId, "Task name 2", "Task description 2");
        Assert.assertEquals("Task name 1", taskService.findOneById(userId, task1.getId()).getName());
        Assert.assertEquals("Task name 2", taskService.findOneById(userId, task2.getId()).getName());
    }

    @Test
    @Category(DBCategory.class)
    public void testRemoveOneById() {
        taskService.removeAll();
        @NotNull TaskDTO task1 = taskService.create(userId, "Task name 1", "Task description 1");
        Assert.assertEquals("Task name 1", taskService.findOneById(userId, task1.getId()).getName());
        Assert.assertEquals(task1.getId(), taskService.removeOneById(userId, task1.getId()).getId());
        Assert.assertEquals(0, taskService.findAll(userId).size());
    }

    @Test
    @Category(DBCategory.class)
    public void testUpdateById() {
        taskService.removeAll();
        @NotNull TaskDTO task1 = taskService.create(userId, "Task name 1", "Task description 1");
        @NotNull final String id = taskService.findAll().get(0).getId();
        Assert.assertEquals("Task name 1", taskService.findOneById(userId, id).getName());
        Assert.assertNotNull(taskService.updateById(userId, id, "Task new name", "Task new description"));
        Assert.assertEquals("Task new name", taskService.findOneById(userId, id).getName());
        Assert.assertEquals("Task new description", taskService.findOneById(userId, id).getDescription());
    }

}
