package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.marker.DBCategory;

@Category(DBCategory.class)
public class ProjectDTOServiceTest extends AbstractDTOServiceTest {

    @NotNull
    private final String userId = "userId-1";

    @NotNull
    private final String userId2 = "userId-2";

    @Test
    public void testChangeProjectStatusById() {
        projectService.removeAll();
        projectService.create(userId, "Project name", "Project description");
        @NotNull final String id = projectService.findAll().get(0).getId();
        Assert.assertEquals(Status.NOT_STARTED, projectService.findOneById(userId, id).getStatus());
        Assert.assertNotNull(projectService.changeProjectStatusById(userId, id, Status.IN_PROGRESS));
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(userId, id).getStatus());
    }

    @Test
    public void testClear() {
        projectService.removeAll();
        @NotNull final ProjectDTO project = new ProjectDTO();
        projectService.create(userId, "Project name 1", "Project description 1");
        projectService.create(userId, "Project name 2", "Project description 2");
        projectService.create(userId2, "Project name 3", "Project description 3");
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
        projectService.removeAll(userId);
        Assert.assertEquals(0, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
    }

    @Test
    public void testCreate() {
        projectService.removeAll();
        projectService.create(userId, "Project name 1", "Project description 1");
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertEquals("Project name 1", projectService.findAll(userId).get(0).getName());
        Assert.assertEquals("Project description 1", projectService.findAll(userId).get(0).getDescription());
        Assert.assertEquals(userId, projectService.findAll().get(0).getUserId());
    }

    @Test
    public void testFindAll() {
        projectService.removeAll();
        projectService.create(userId, "Project name 1", "Project description 1");
        projectService.create(userId, "Project name 2", "Project description 2");
        projectService.create(userId2, "Project name 3", "Project description 3");
        Assert.assertEquals(2, projectService.findAll(userId).size());
        Assert.assertEquals(1, projectService.findAll(userId2).size());
        Assert.assertEquals(3, projectService.findAll().size());
    }

    @Test
    public void testFindOneById() {
        projectService.removeAll();
        @NotNull ProjectDTO project1 = projectService.create(userId, "Project name 1", "Project description 1");
        @NotNull ProjectDTO project2 = projectService.create(userId, "Project name 2", "Project description 2");
        Assert.assertEquals("Project name 1", projectService.findOneById(userId, project1.getId()).getName());
        Assert.assertEquals("Project name 2", projectService.findOneById(userId, project2.getId()).getName());
    }

    @Test
    public void testRemoveOneById() {
        projectService.removeAll();
        @NotNull ProjectDTO project1 = projectService.create(userId, "Project name 1", "Project description 1");
        Assert.assertEquals("Project name 1", projectService.findOneById(userId, project1.getId()).getName());
        Assert.assertEquals(project1.getId(), projectService.removeOneById(userId, project1.getId()).getId());
        Assert.assertEquals(0, projectService.findAll(userId).size());
    }

    @Test
    public void testUpdateById() {
        projectService.removeAll();
        @NotNull ProjectDTO project1 = projectService.create(userId, "Project name 1", "Project description 1");
        @NotNull final String id = projectService.findAll().get(0).getId();
        Assert.assertEquals("Project name 1", projectService.findOneById(userId, id).getName());
        Assert.assertNotNull(projectService.updateById(userId, id, "Project new name", "Project new description"));
        Assert.assertEquals("Project new name", projectService.findOneById(userId, id).getName());
        Assert.assertEquals("Project new description", projectService.findOneById(userId, id).getDescription());
    }

}
