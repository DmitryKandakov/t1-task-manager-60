package ru.t1.dkandakov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkandakov.tm.api.repository.dto.IUserOwnerRepositoryDTO;
import ru.t1.dkandakov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnerDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractDtoRepository<M> implements IUserOwnerRepositoryDTO<M> {

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        entityManager.merge(model);
    }

}
