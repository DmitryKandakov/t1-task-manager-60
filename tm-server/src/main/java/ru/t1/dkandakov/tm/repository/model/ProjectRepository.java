package ru.t1.dkandakov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkandakov.tm.api.repository.model.IProjectRepository;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.model.Project;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public List<Project> findAll() {
        @NotNull final String jpql = "SELECT m FROM Project m";
        return entityManager.createQuery(jpql, Project.class).getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAll(@NotNull final String userId, @NotNull final ProjectSort sort) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId ORDER BY " + sort.getColumnName();
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String id) {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.id = :id AND m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        @NotNull final String jpql = "DELETE FROM Project m WHERE m.id = :projectId AND m.user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("projectId", project.getId())
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        Optional<Project> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        Optional<Project> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }


    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
