package ru.t1.dkandakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkandakov.tm.api.repository.model.IRepository;
import ru.t1.dkandakov.tm.api.service.model.IService;
import ru.t1.dkandakov.tm.exception.entity.EntityNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.ValueIsNullException;
import ru.t1.dkandakov.tm.exception.field.IdEmptyException;
import ru.t1.dkandakov.tm.exception.field.IndexIncorrectException;
import ru.t1.dkandakov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    @Autowired
    IRepository<M> repository;


    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M add(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@Nullable Collection<M> models) {
        if (models == null) throw new ValueIsNullException();

        for (final M model : models) {
            add(model);
        }

        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models == null) throw new ValueIsNullException();
        removeAll();
        add(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return (M) repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<M> models = findAll();
        return models.get(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeOne(@Nullable M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M model = findOneById(id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    @Transactional
    public M removeOneByIndex(@Nullable Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @Nullable M model = findOneByIndex(index);
        if (model == null) return null;
        return removeOne(model);
    }


    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) return false;
        return findOneById(id) != null;
    }

}
