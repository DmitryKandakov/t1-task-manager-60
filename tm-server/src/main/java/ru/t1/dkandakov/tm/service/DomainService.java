package ru.t1.dkandakov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dkandakov.tm.api.service.IDomainService;
import ru.t1.dkandakov.tm.api.service.IServiceLocator;
import ru.t1.dkandakov.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@AllArgsConstructor
public class DomainService implements IDomainService {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
    }

    @Override
    @SneakyThrows
    public void loadDataBackup() {
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBackup() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBase64() {
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64Data = new String(base64Byte);
        @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBase64() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBinary() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBinary() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataJsonFasterXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataJsonFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);

        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataJsonJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @NotNull final File file = new File(FILE_JSON);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataJsonJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);

        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataXmlFasterXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataXmlFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);

        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataXmlJaxB() {
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataXmlJaxB() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_XML);

        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataYamlFasterXml() {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
        @NotNull final String json = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataYamlFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_YAML);

        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
