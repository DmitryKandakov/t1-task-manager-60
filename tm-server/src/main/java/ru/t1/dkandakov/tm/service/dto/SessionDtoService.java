package ru.t1.dkandakov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.dkandakov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.dkandakov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.dkandakov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionRepositoryDTO> implements ISessionServiceDTO {

    @NotNull
    public ISessionRepositoryDTO getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(ISessionRepositoryDTO.class);
    }

}
