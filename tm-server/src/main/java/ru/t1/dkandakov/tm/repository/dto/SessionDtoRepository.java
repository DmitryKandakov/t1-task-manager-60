package ru.t1.dkandakov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkandakov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.dkandakov.tm.dto.model.SessionDTO;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDtoRepository extends AbstractUserOwnerDtoRepository<SessionDTO> implements ISessionRepositoryDTO {


    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class).getResultList();
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final SessionDTO session) {
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.id = :sessionId AND m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("sessionId", session.getId())
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        Optional<SessionDTO> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        Optional<SessionDTO> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM SessionDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
