package ru.t1.dkandakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.enumerated.Status;

import java.util.List;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    );

    @NotNull
    ProjectDTO create(
            @Nullable final String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO updateById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull Status status
    );

    @NotNull
    ProjectDTO changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull Status status
    );

}
