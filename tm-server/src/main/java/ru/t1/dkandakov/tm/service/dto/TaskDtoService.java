package ru.t1.dkandakov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkandakov.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1.dkandakov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.dkandakov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.dkandakov.tm.dto.model.TaskDTO;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.enumerated.TaskSort;
import ru.t1.dkandakov.tm.exception.entity.EntityNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.StatusEmptyException;
import ru.t1.dkandakov.tm.exception.entity.TaskNotFoundException;
import ru.t1.dkandakov.tm.exception.field.*;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, ITaskRepositoryDTO> implements ITaskServiceDTO {

    @NotNull
    @Autowired
    public ITaskRepositoryDTO repository;
    @NotNull
    @Autowired
    IProjectServiceDTO projectService;

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final TaskSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setStatus(Status.NOT_STARTED);

        return add(userId, task);
    }

    @NotNull
    @SneakyThrows
    @Transactional
    private TaskDTO update(@Nullable final TaskDTO task) {
        if (task == null) throw new ProjectNotFoundException();

        repository.update(task);

        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return update(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return update(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);

        return update(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);

        return update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);

        update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        System.out.println(task.getProjectId());
        System.out.println(projectId);
        if (task == null || !projectId.equals(task.getProjectId())) throw new TaskNotFoundException();
        task.setProjectId(null);

        update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        repository.removeTasksByProjectId(userId, projectId);
        projectService.removeOneById(userId, projectId);
    }

}
