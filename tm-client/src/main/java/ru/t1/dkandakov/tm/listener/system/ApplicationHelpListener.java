package ru.t1.dkandakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.listener.AbstractListener;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    public static final String ARGUMENT = "-h";

    public static final String DESCRIPTION = "Display list of terminal commands.";

    public static final String NAME = "help";

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@Nullable final AbstractListener abstractListener : listeners) {
            if (abstractListener == null) continue;
            System.out.println(abstractListener.toString());
        }
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}