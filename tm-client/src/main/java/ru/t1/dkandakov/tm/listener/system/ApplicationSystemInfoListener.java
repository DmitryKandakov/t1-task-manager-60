package ru.t1.dkandakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.FormatUtil;

@Component
public final class ApplicationSystemInfoListener extends AbstractSystemListener {

    @Override
    @EventListener(condition = "@applicationSystemInfoListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final Runtime runtime = Runtime.getRuntime();
        @Nullable final long availableProcessors = runtime.availableProcessors();
        @Nullable final long freeMemory = runtime.freeMemory();
        @Nullable final String freeMemoryFormat = FormatUtil.format(freeMemory);
        @Nullable final long maxMemory = runtime.maxMemory();
        @Nullable final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        @Nullable final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory));
        @Nullable final long totalMemory = runtime.totalMemory();
        @Nullable final String totalMemoryFormat = FormatUtil.format(totalMemory);
        @Nullable final long usageMemory = totalMemory - freeMemory;
        @Nullable final String usageMemoryFormat = FormatUtil.format(usageMemory);

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show system information.";
    }
}