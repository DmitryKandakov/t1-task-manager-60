package ru.t1.dkandakov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;


@NoArgsConstructor
public final class ProjectGetByIndexResponse extends AbstractProjectResponse {

    public ProjectGetByIndexResponse(@Nullable ProjectDTO project) {
        super(project);
    }

}
