package ru.t1.dkandakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Cacheable
@Table(name = "tm_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User extends AbstractModel {

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();


    @Nullable
    @Column(name = "login")
    private String login;

    @Nullable
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "fst_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "mid_name")
    private String middleName;

    @NotNull
    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "locked")
    private boolean locked = false;

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final Role role
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public boolean isLocked() {
        return locked;
    }

}