package ru.t1.dkandakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIdRequest;
import ru.t1.dkandakov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIdRequest extends AbstractIdRequest {

    @Nullable
    private Status status;

    public ProjectCompleteByIdRequest(
            @Nullable String token,
            @Nullable String id,
            @Nullable Status status
    ) {
        super(token, id);
        this.status = status;
    }

}