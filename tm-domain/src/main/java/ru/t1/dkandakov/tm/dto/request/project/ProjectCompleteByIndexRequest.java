package ru.t1.dkandakov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIndexRequest;
import ru.t1.dkandakov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private Status status;

    public ProjectCompleteByIndexRequest(
            @Nullable String token,
            @Nullable Integer index,
            @Nullable Status status
    ) {
        super(token, index);
        this.status = status;
    }

}