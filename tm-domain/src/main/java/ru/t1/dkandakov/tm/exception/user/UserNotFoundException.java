package ru.t1.dkandakov.tm.exception.user;

public class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Error! User does not exist...");
    }

}
